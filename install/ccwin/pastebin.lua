srcFiles = {
   "/server",
   "/startup",
   "/license.txt",
   "/win/win",
   "/win/startup.ini",
   "/win/term/startup.ini",
   "/win/apps/browse",
   "/win/apps/browse.ini",
   "/win/apps/chat",
   "/win/apps/cmd",
   "/win/apps/email",
   "/win/apps/email.ini",
   "/win/apps/emread",
   "/win/apps/emwrite",
   "/win/apps/fexplore",
   "/win/apps/fexplore.asc",
   "/win/apps/manager",
   "/win/apps/notepad",
   "/win/apps/notepad.ini",
   "/win/apps/sadmin",
   "/win/apps/sadmin.ini",
   "/win/apps/shutdown",
   "/win/apis/cmndlg",
   "/win/apis/html"
}

if not http then
   printError("to install ccwindows 0.23, you'll need the http api. in a server? ask a server moderator to enable the http api.")
end

local function backup(path)
   if fs.exists(path) then
      print("backing up "..path)
      if fs.exists("/backup"..path) then
         fs.delete("/backup"..path)
         printError(path .. " has been overwritten")
      end
      fs.copy(path, "/backup"..path)
   end
end
backup("/startup")
backup("/win/startup.ini")
backup("/win/apps/notepad.ini")
backup("/win/apps/fexplore.asc")
backup("/win/apps/browse.ini")
backup("/win/apps/email.ini")
backup("/win/apps/sadmin.ini")
backup("/win/term/desktop.ini")
backup("/win/term/startup.ini")
backup("/win/term/theme.ini")

local function get(url)
    -- Check if the URL is valid
    local ok, err = http.checkURL(url)
    if not ok then
        return
    end

    local response = http.get(url, nil, true)
    if not response then
        return nil
    end

    local sResponse = response.readAll()
    response.close()
    return sResponse
end


for i = 1, #srcFiles, 1 do
   write("downloading "..srcFiles[i])
   local hOut = io.open(srcFiles[i], "w")
   if not hOut then
      print("")
      print("could not open "..srcFiles[i])
      write("run the command again to retry")
      if fs.exists("/backup") then print(", but first you must move any backups you have that were moved and save them if you want") else print(", currently you don't have any backups, so you don't need to move any backups for stoarge.") end
      return
   end
 
   parallel.waitForAny(function()
   local text = get("https://gitlab.com/xenussoft-computercraft-organization/ccwindows/-/raw/master/root/win" .. srcFiles[i])
   hOut:write(text)
   end, function()
      while true do
         write(".")
         sleep(0.3)
      end
   end)
   print("")
   hOut:close()
end 
